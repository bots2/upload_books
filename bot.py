#!/usr/bin/python3

import logging
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler, Filters

import epub_data
from config import Config

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def epub_file(update: Update, context: CallbackContext) -> None:
    file_is = update.message.document.file_id
    context.bot.get_file(file_is).download('file.epub')
    epub_data.book = epub_data.epub.read_epub('file.epub')
    logger.info(epub_data.get_filename())
    context.bot.send_photo(chat_id=update.effective_chat.id, photo=epub_data.save_cover_image_book(return_content=True))
    context.bot.send_document(chat_id=update.effective_chat.id, document=update.message.document)
    context.bot.send_message(chat_id=update.effective_chat.id, text=epub_data.get_msg())


def get_initialized_updater() -> Updater:
    updater = Updater(Config.TOKEN)
    epub_handler = MessageHandler(Filters.attachment, epub_file)
    updater.dispatcher.add_handler(epub_handler)

    if Config.START_WEBHOOK:
        updater.start_webhook(listen="0.0.0.0", port=Config.PORT, url_path=Config.TOKEN)
        updater.bot.setWebhook(f"https://{Config.PYTHON_ANYWHERE_USERNAME}.pythonanywhere.com/{Config.TOKEN}")
    else:
        updater.start_polling()

    return updater


get_initialized_updater().idle()
