import os


class Config:
    TOKEN: str = os.getenv("TOKEN")
    PYTHON_ANYWHERE_USERNAME: str = os.getenv("PYTHON_ANYWHERE_USERNAME")
    PORT: int = int(os.getenv("PORT", 3000))
    START_WEBHOOK: bool = bool(os.getenv("START_WEBHOOK", False))
