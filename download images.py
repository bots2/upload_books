from urllib.error import HTTPError
import urllib.request
import json

with open('books.json', "r") as f:
    books = json.load(f)

for book in books:
    if book["image"]:
        print(book["image"])
        try:
            urllib.request.urlretrieve(book["image"], "./images/" + book["image"].split("/")[-1])
        except HTTPError as error:
            if error.code == 404:
                print("404")
            else:
                print(error)
