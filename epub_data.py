#!/usr/bin/python3
import re
import string

from markdownify import markdownify as md
from ebooklib import epub
import requests as r
from parsel import Selector
import json

import_file = 'books.json'
json_books = ''
book = None

with open(import_file, 'rb') as f:
    json_books = json.load(f)
    
class GetBooks():
    '''
        url = 'https://www.getbooks.co.il/%D7%A4%D7%A0%D7%92-%D7%94%D7%9C%D7%91%D7%9F/'
        print('==================================')
        print(f'{GetBooks(url).tags}')
        print('==================================')
    '''

    def __init__(self, url):
        self.url = url
        self.page = self.get_page(self.url)
        self.tags = self.get_tags(self.page)
        self.image = self.get_image(self.page)
        self.number_pages = self.get_number_pages(self.page)

    def get_tags(self, page):
        tags = ''
        xpath = '/html/body/div[11]/div/div[1]/div[5]/div/div[2]/div[4]/div[1]/form/div[5]/div[8]/div[1]/a//text()'
        selector = Selector(text=page)
        tag_list = [f'#{tag.strip()}' for tag in selector.xpath(xpath).getall()]
        tags = '\n'.join(tag_list)
        tags = tags.replace(", ", "\n#")
        tags = tags.replace(" ", "_")
        return tags

    def get_image(self, page):
        xpath = '/html/body/div[11]/div/div[1]/div[5]/div/div[2]/div[4]/div[1]/form/div[2]/img/@src'
        selector = Selector(text=page)
        url = selector.xpath(xpath).get()
        image = r.get(url, stream=True).raw
        return image.read()

    def get_number_pages(self, page):
        xpath = '/html/body/div[11]/div/div[1]/div[5]/div/div[2]/div[4]/div[1]/form/div[5]/div[8]/div[2]/div[2]/strong//text()'
        selector = Selector(text=page)
        size = selector.xpath(xpath).get()
        return size

    def get_page(self, url):
        page = r.get(self.url).text
        return page

def remove_niqqud_from_string(my_string):
    return ''.join(['' if  1456 <= ord(c) <= 1479 else c for c in my_string])


def make_key(book_name):
    book_name = remove_niqqud_from_string(book_name)
    book_name = re.sub('\W+', '', book_name)
    return ''.join(sorted(list(set(book_name))))


def search_book_in_json(book_name):
    key_book = make_key(book_name)
    for i in range(1, 21197):
        if json_books[i].get('key') == key_book:
            return json_books[i]


def get_tags_from_json(book_name: str):
    tags = ''
    book = search_book_in_json(book_name)
    if book:
        tags = book.get('tags', tags)
        tag_list = [f'#{tag.strip()}' for tag in tags]
        if tag_list:
            tags = '\n'.join(tag_list)
            tags = tags.replace(", ", "\n#")
            tags = tags.replace(" ", "_")
            tags = tags.replace('מד"ב', 'מדע_בדיוני')
    return tags


def get_title():
    title = book.get_metadata('DC', 'title')
    return f'{title[0][0]}' if title else ""


def get_creator():
    creator = book.get_metadata('DC', 'creator')
    return creator[0][0] if creator else ""


def get_date():
    date = book.get_metadata('DC', 'date')
    year = ''
    if date:
        year = date[0][0].split('-')[0]
    return f'({year})' if year else ""


def get_publisher():
    publisher = book.get_metadata('DC', 'publisher')
    return publisher[0][0] if publisher else "הוצאה לא ידועה"


def get_description():
    description = book.get_metadata('DC', 'description')
    return description[0][0] if description else ""


def get_cover_image_by_name():
    cover_name = get_cover_image_name()
    if cover_name:
        return book.get_item_with_id(cover_name)


def get_cover_image_name():
    for name in ['cover-image', 'cover', 'Cover']:
        cover_image = book.get_item_with_id(name)
        if cover_image:
            if isinstance(cover_image, (epub.EpubCover, epub.EpubImage)):
                return name
            else:
                name = re.search('\w+.(jpg|jpeg|png)', str(cover_image.content))
                return name.group(0)


def get_cover_image_by_type():
    cover_name = get_cover_image_name()
    cover_name = cover_name or 'cover'
    for image in book.get_items_of_type(1):
        if cover_name in image.get_name():
            return image


def get_cover_image_book():
    for func in [get_cover_image_by_name, get_cover_image_by_type]:
        cover_image = func()
        if cover_image:
            return cover_image


def save_cover_image_book(return_content=False):
    cover_image = get_cover_image_book()
    if cover_image:
        if '.' in cover_image.file_name:
            file_type = cover_image.file_name.rsplit('.')[1]
        else:
            file_type = 'jpg'
        filename = f'{get_title()}.{file_type}'
        valid_chars = f"-_.() 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ%sאבגדהוזחטיכלמםנןסעפףצץקרשת"
        filename = "".join(x for x in filename if x in valid_chars)
        if return_content:
            return cover_image.content
        with open(filename, 'wb') as f:
            f.write(cover_image.content)
        return filename


def get_msg():
    title = get_title()
    creator = f'/ {get_creator()}' if get_creator() else ''
    publisher = f'הוצאה: {get_publisher()} {get_date()}'
    desc = get_description()
    return md(f'{title} {creator}\n\n{publisher}\n\n{desc}\n{get_tags_from_json(title)}'.replace('<p>','\n\n'))


def get_filename():
    title = get_title()
    creator = f'{get_creator()}' if get_creator() else ''
    return f"{title} - {creator}.epub"


def open_book(filename):
    global book
    book = epub.read_epub(filename)


def main(filename):
    open_book(filename)
    file_name = get_filename()
    msg = get_msg()
    return file_name, msg, save_cover_image_book()


if __name__ == '__main__':
    main(sys.argv[1])
