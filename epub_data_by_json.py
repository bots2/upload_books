#!/usr/bin/python3
import json
import os

import re

import html2markdown
import ebooklib
from ebooklib import epub
import requests as r
from parsel import Selector

from markdownify import markdownify as md
from upload_books import epub_data

book = None

THIS_FOLDER = "/".join(os.path.realpath(__file__).split("/")[:-1])
with open(f'{THIS_FOLDER}/books.json', "r") as f:
    books_list = json.load(f)
book_json = ""


def get_book_by_name(book_name: str):
    key_book = make_key(book_name)
    for i in range(1, 21197):
        if books_list[i].get('key') == key_book:
            return books_list[i]


def remove_niqqud_from_string(book_name: str):
    return ''.join(['' if  1456 <= ord(c) <= 1479 else c for c in book_name])


def make_key(book_name: str):
    book_name = remove_niqqud_from_string(book_name)
    book_name = re.sub('\W+', '', book_name)
    return ''.join(sorted(list(set(book_name))))


# def get_book_by_name(book_name: str):
#     key = ""
#     for char in book_name:
#         # hebrew or number or english...
#         if 1514 >= ord(char) >= 1488 or 57 >= ord(char) >= 48 or 90 >= ord(char) >= 65 or 122 >= ord(
#                 char) >= 97:
#             if char not in key:
#                 key += char

#     key = "".join(sorted(key))
#     print("key:", key)
#     for book_jsn in books_list:
#         if book_jsn['key'] == key:
#             return book_jsn
#     return None

def get_tags_from_json(book_name: str):
    tags = ''
    book = get_book_by_name(book_name)
    if book:
        tags = book.get('tags', tags)
        tag_list = [f'#{tag.strip()}' for tag in tags]
        if tag_list:
            tags = '\n'.join(tag_list)
            tags = tags.replace(", ", "\n#")
            tags = tags.replace(" ", "_")
    return tags
    
def get_title():
    title = book.get_metadata('DC', 'title')
    return f'{title[0][0]}' if title else ""


def get_creator():
    creator = book.get_metadata('DC', 'creator')
    return creator[0][0] if creator else ""


def get_date():
    global book_json
    year = book_json['year']
    return f'({year})' if year else ""


def get_publisher():
    global book_json
    publisher = "".join(book_json['publisher'])
    return publisher if publisher else "הוצאה לא ידועה"


def get_number_of_pages():
    global book_json
    num_page = "".join(book_json['num_page'])
    return num_page if num_page else ""


def get_description():
    global book_json
    description = book_json['description']
    return description if description else ""


def get_cover_image_link():
    global book_json
    return book_json['image']


def get_msg():
    global book_json
    title = get_title()
    creator = f'/ {get_creator()}' if get_creator() else ''
    publisher = f'הוצאה: {get_publisher()} {get_date()}'
    number_of_pages = f'/ {get_number_of_pages()} עמודים' if get_number_of_pages() else ''
    desc = get_description()
    return md(f'{title} {creator}\n\n{publisher} {number_of_pages}\n\n{desc}\n{get_tags_from_json(title)}')


def get_filename():
    title = get_title()
    creator = f'{get_creator()}' if get_creator() else ''
    return f"{title} - {creator}.epub"


def open_book(filename):
    global book
    global book_json
    book = epub.read_epub(filename)
    book_json = get_book_by_name(get_title())


def main(filename):
    global book_json
    open_book(filename)
    if not book_json:
        return epub_data.main(filename)
    file_name = get_filename()
    msg = get_msg()
    return file_name, msg, get_cover_image_link()


if __name__ == '__main__':
    main(sys.argv[1])
