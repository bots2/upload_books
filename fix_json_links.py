import json

with open('books.json', "r") as f:
    books = json.load(f)

for book in books:
    if book["image"]:
        book["image"] = "https://images-evrit.yit.co.il/" + book["image"].replace("/Image_", "/").replace("/image_", "/")

with open('books.json', "w", encoding="utf-8") as f:
    json.dump(books, f, indent=4, ensure_ascii=False)
